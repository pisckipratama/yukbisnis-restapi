const mongoose = require('mongoose');
const { ObjectId } = mongoose.Schema.Types;
const Schema = mongoose.Schema;

const WalletSchema = new Schema({
  tanggal: String,
  tipe: String,
  nominal: String,
  keterangan: String,
  ownedBy: {
    type: ObjectId,
    ref: "UserYukBisnis"
  }
});

module.exports = mongoose.model("HistoryYukBisnis", WalletSchema);

