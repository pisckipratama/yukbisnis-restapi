const mongoose =require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  saldo_akhir: {
    type: String
  }
});

module.exports = mongoose.model("UserYukBisnis", UserSchema);