var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var { JWT_SECRET } = require('../config/key');
var UserSchema = require('../models/Users');
var jwtAuth = require('../auth/jwtAuth');

/* GET users listing. */
router.get('/', jwtAuth, async (req, res, next) => {
  try {
    const getAllUsers = await UserSchema.find();
    res.status(200).json(getAllUsers);
  } catch (error) {
    res.status(500).json(error.message);
  }
});

router.post('/', async (req, res, next) => {
  try {
    const { username, name, password } = req.body;
    const newUser = new UserSchema({ username, name, password });
    const saveUser = await newUser.save();
    res.status(201).json(saveUser);
  } catch (error) {
    res.status(500).json(error.message);
  }
});

router.post('/login', async (req, res, next) => {
  const { username, password } = req.body;
  
  try {
    const chkUsername = await UserSchema.findOne({ username });
    if (!chkUsername) return res.status(400).json({ success: false, code: 400, message: "Email or Password incorrect!" });

    const isMatch = (password == chkUsername.password);
    if (!isMatch) return res.status(400).json({ success: false, code: 400, message: "Email or Password incorrect!" });

    const payload = { user: { _id: chkUsername._id, name: chkUsername.name, username: chkUsername.username, saldo: chkUsername.saldo_akhir } };
    jwt.sign(payload, JWT_SECRET, { expiresIn: 3600 }, (err, token) => {
      if (err) throw err;
      res.status(200).json({ success: true, code: 200, data: { user: { _id: chkUsername._id, username: chkUsername.username, name: chkUsername.name }, token }, message: "login successfully" });
    });

  } catch (error) {
    res.send(error)
  }
})

module.exports = router;
