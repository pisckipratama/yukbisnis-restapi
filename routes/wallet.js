const express = require('express');
const router = express.Router();
const WalletSchema = require('../models/Wallet');
const jwtAuth = require('../auth/jwtAuth');

router.get('/', jwtAuth, async (req, res) => {
  try {
    const result = await WalletSchema.find().populate('ownedBy', '_id name username');
    return res.status(200).json({ success: true, code: 200, message: "Retrive all data successfully.", data: result })
  } catch (err) {
    console.error(err.message);
    return res.send(err.message);
  }
});

router.post('/', jwtAuth, async (req, res) => {
  const { tanggal, tipe, nominal, keterangan } = req.body;
  try {
    const newWallet = new WalletSchema({ tanggal, tipe, nominal, keterangan, ownedBy: req.user });
    const saveRecord = await newWallet.save();
    return res.status(201).json({ success: true, code: 201, data: saveRecord, message: "created successfully." });
  } catch (error) {
    console.error(err.message);
    return res.send(err.message);
  }
});

router.get('/myhistory', jwtAuth, async(req, res) => {
  try {
    const myHistory = await WalletSchema.find({ ownedBy: req.user._id }).populate("ownedBy", "_id username name");
    return res.status(200).json({ success: true, code: 200, message: "Retrive data successfully.", data: myHistory });
  } catch (err) {
    console.error(err.message);
    return res.send(err.message);
  }
});

module.exports = router;